"use strict";
let moment = require('moment');

exports.isValidDate = (date, format) => {
    return date ? moment(date, format).isValid() : false ;
};

exports.isSameOrBefore = (start, end, format ) => {
    return moment(start, format).isSameOrBefore(moment(end, format));
}

exports.startOfMonth = (date, format) => {
    return moment(date, format).isSame(moment(date, format).clone().startOf('month'), 'day')
}

exports.endOfMonth = (date, format) => {
    return moment(date,format).isSame(moment(date, format).clone().endOf('month'), 'day')
}

exports.format = (m, format) => {
    return m.format(format);
}