"use strict";

exports.errorCodes = {
    badRequest: 400,
    internalServerError: 500,
    notFound: 404,
    ok: 200
};