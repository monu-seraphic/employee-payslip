module.exports = {
    start: "2017-07-01",
    end: "2018-06-30",
    brackets: [
        { bottom:      0, top:    18200, base:     0, threshold:      0, rate: 0 },
        { bottom:  18201, top:    37000, base:     0, threshold:  18200, rate: 0.19 },
        { bottom:  37001, top:    87000, base:  3572, threshold:  37000, rate: 0.325 },
        { bottom:  87001, top:   180000, base: 19822, threshold:  87000, rate: 0.37 },
        { bottom: 180001, top: Infinity, base: 54232, threshold: 180000, rate: 0.45 },
    ],
}