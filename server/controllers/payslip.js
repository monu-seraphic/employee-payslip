"use strict";

let errorCodes = require('./utils').errorCodes,
    moment = require('moment'),
    helperFunctions = require('./heplers-functions'),
    taxRateList = require('./taxRateList'),
    _ = require('underscore');

exports.calculatePayslip = (req, res) => {
    let { startDate, endDate, annualPay, firstName, lastName, superRate } = req.body,
        dateFormat = 'YYYY-MM-DD';


    if (!startDate) {
        return res.status(errorCodes.badRequest).send({
            message: 'Start date is required.'
        })
    }

    if (!endDate) {
        return res.status(errorCodes.badRequest).send({
            message: 'End date is required.'
        })
    }

    if (!firstName) {
        return res.status(errorCodes.badRequest).send({
            message: 'First Name is required.'
        })
    }

    if (!lastName) {
        return res.status(errorCodes.badRequest).send({
            message: 'Last Name is required.'
        })
    }

    if (!annualPay) {
        return res.status(errorCodes.badRequest).send({
            message: 'Annual Salary is required.'
        })
    }

    if (!superRate) {
        return res.status(errorCodes.badRequest).send({
            message: 'Super Rate is required.'
        })
    }
    
    if(!helperFunctions.isValidDate(startDate, dateFormat)) {
        return res.status(errorCodes.badRequest).send({
            message: 'Start date is not a valid date.'
        })
    }

    if(!helperFunctions.isValidDate(endDate, dateFormat)) {
        return res.status(errorCodes.badRequest).send({
            message: 'End date is not a valid date.'
        })
    }

    if(!helperFunctions.isSameOrBefore(startDate, endDate, dateFormat)) {
        return res.status(errorCodes.badRequest).send({
            message: 'Start date must be brfore end date.'
        })
    }

    let startDateMoment = moment(startDate, dateFormat),
        endDateMoment = moment(endDate, dateFormat),
        payslips = [];

    // Set start date to be first date of month
    startDateMoment.set('date', startDateMoment.clone().startOf('month').format('DD'));  
    
    // Set end date to be end date of month
    endDateMoment.set('date', endDateMoment.clone().endOf('month').format('DD')); 

    for (let i = 0; i <= endDateMoment.diff(startDateMoment, 'month'); i++) {

        const monthStart = startDateMoment.clone().add(i, 'month');
        const monthEnd = monthStart.clone().endOf('month');

        function findPeriod(m) {
            return m.isSameOrAfter(taxRateList.start) && m.isSameOrBefore(taxRateList.end);
        }

        const taxRatePeriod = findPeriod(monthStart);
        
        // could not find tax period
        if (!taxRatePeriod) {
            break;
        }

        const bracket = _.find(taxRateList.brackets, (b) => annualPay >= b.bottom && annualPay <= b.top);
        const gross = Math.round(annualPay / 12);
        const tax = Math.round((bracket.base + (annualPay - bracket.threshold) * bracket.rate) / 12);

        payslips.push({
            fullName: `${firstName} ${lastName}`,
            period: `${helperFunctions.format(monthStart, 'YYYY-MM-DD')} - ${helperFunctions.format(monthEnd, 'YYYY-MM-DD')}`,
            gross: gross,
            tax: tax,
            net: gross - tax,
            super: Math.round(gross * superRate / 100),
        });bracket
    }
    // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    console.log('payslips', payslips)
    res.status(errorCodes.ok).send(payslips);
}