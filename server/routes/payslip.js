"use strict";

let express = require('express'),
    router = express.Router(),
    controllers = require('../controllers');

router.post('/payslip', controllers.payslipController.calculatePayslip);

module.exports = router;
