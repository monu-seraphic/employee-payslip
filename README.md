# Project Title

Employee payslip for a flexible pay cycle

### Prerequisites
- Node
- Nodemon installed globally: $ npm install -g nodemon

### Installing
- Clone the repo and `cd employee-payslip`
- Run `npm install` to install all the dependencies.
- Run `npm start`. This will start a node server on port 4000 and resct development server on port 3000
- Open `http://localhost:3000` in browser. There will be a from for adding input
- Click `Submit` button. This will show you result in right hand section of page.
## Tests Cases
Inputs
----------------

(first name, last name, annual salary, super rate (%), payment period):  
David,Rudd,60050,9%,01 March - 31 March  
Ryan,Chen,120000,10%,01 March - 31 March  

Outputs
-----------------

(name, pay period, gross income, income tax, net income, super):  
David Rudd,01 March - 31 March,5004,922,4082,450  
Ryan Chen,01 March - 31 March,10000,2669,7331,1000
