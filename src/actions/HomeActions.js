import alt from '../altInstance';
import Agent from '../services/requestInstance';
import config from '../config/config';

const BACKEND_URL = config.BACKEND_URL,
      AgentInstance = new Agent();

class HomeActions {
    calculateSlips(payload, cb) {
        return (dispatch) => {
            AgentInstance
            .fire('post', `${BACKEND_URL}/payslip`)
            .send(payload)
            .end((err, res) => {
                dispatch({
                    error: err,
                    data: res && res.body
                });
                if(typeof cb === 'function') return cb(err);
            });
        }
    };
}

export default alt.createActions(HomeActions);
