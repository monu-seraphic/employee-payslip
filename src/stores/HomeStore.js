import alt from '../altInstance';
import { HomeActions } from '../actions';

class HomeStore {
  constructor() {
    this.salarySlips = [];
    this.bindListeners({
      calculateSlips: HomeActions.CALCULATE_SLIPS,
    });
  };

  calculateSlips (payload) {
    if(!payload.error) {
        this.salarySlips = (payload.data);
    }
  }
}

export default alt.createStore(HomeStore, 'HomeStore');
