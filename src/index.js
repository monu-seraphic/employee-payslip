import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { HomeComponent } from './Components';

ReactDOM.render(<HomeComponent />, document.getElementById('root'));
