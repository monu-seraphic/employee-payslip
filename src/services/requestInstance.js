import http from 'superagent';

class Agent {
    fire(method, url) {
        let defaultHeaders = {
            // 'Content-Type': 'application/json'
        }
        return http[method](url).set(defaultHeaders);
    }
};

export default Agent;
