import React, { Component } from 'react';
import { HomeStore } from '../stores';
import { HomeActions } from '../actions';

class HomeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      startDate: '',
      endDate: '',
      annualPay: '',
      lastName: '',
      superRate: '',
      payslipInfo : {
        fullName: '',
        period: '',
        gross:'',
        tax: '',
        net: '',
        super: ''
      }
    };
    this.handleHomeStore = this.handleHomeStore.bind(this);
    this.handleUserInput = this.handleUserInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentWillMount() {
    HomeStore.listen(this.handleHomeStore);
  }

  handleHomeStore(state) {
    let valueToSet = state.salarySlips[0] || {};
    this.setState({'payslipInfo': valueToSet})
  }

  handleUserInput(e) {
    let name = e.target.name,
      value = e.target.value;
    this.setState({ [name]: value });
  }

  handleSubmit(event) {

    event.preventDefault();
    
    let { startDate, endDate, annualPay, firstName, lastName, superRate } = this.state;

    
    if (!startDate) {
      return alert('Start date is required.')
    }

    if (!endDate) {
      return alert('End date is required.')
    }

    if (!annualPay) {
      return alert('Annual Pay is required.')
    }

    if (!firstName) {
      return alert('First Name is required.')
    }

    if (!lastName) {
      return alert('Last date is required.')
    }

    if (!superRate) {
      return alert('Super Rate is required.')
    }
    
    HomeActions.calculateSlips({
      startDate, endDate, annualPay, firstName, lastName, superRate
    })
  }
  render() {
    let {payslipInfo} = this.state;
    return (
      <section className="emi-section">
        <div className="container">
          <div className="emi-container">
            <h1>Tax Calculate</h1>
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="col-sm-6">
                  <div className="form-container">
                    <div className="form-group">
                      <label>First Name</label>
                      <input
                        type="text"
                        className="form-control term"
                        name="firstName"
                        id="firstName"
                        value={this.state.firstName}
                        onChange={this.handleUserInput}
                      />
                    </div>
                    <div className="form-group">
                      <label>Last Name</label>
                      <input
                        type="text"
                        className="form-control term"
                        name="lastName"
                        id="lastName"
                        value={this.state.lastName}
                        onChange={this.handleUserInput}
                      />
                    </div>
                    <div className="form-group">
                      <label>Start From</label>
                        <input
                          type="date"
                          className="form-control"
                          name="startDate"
                          id="startDate"
                          value={this.state.startDate}
                          onChange={this.handleUserInput}
                        />
                    </div>
                    <div className="form-group">
                      <label>To</label>
                        <input
                          type="date"
                          className="form-control"
                          name="endDate"
                          id="endDate"
                          value={this.state.endDate}
                          onChange={this.handleUserInput}
                        />
                    </div>
                    <div className="form-group">
                      <label>Annual Salary</label>
                      <div className="input-group"> <span className="input-group-addon" id="basic-addon2">$</span>
                        <input
                          type="text"
                          className="form-control principal"
                          name="annualPay"
                          id="annualPay"
                          value={this.state.annualPay}
                          onChange={this.handleUserInput}
                        />
                      </div>
                      <div id="slider-range-min"></div>
                    </div>
                    <div className="form-group">
                      <label>Super Rate</label>
                      <div className="input-group">
                        <input
                          type="number"
                          className="form-control interest"
                          name="superRate"
                          value={this.state.superRate}
                          onChange={this.handleUserInput}
                        />
                        <span className="input-group-addon" id="basic-addon2">%</span> </div>
                      <div id="slider-range-min1"></div>
                    </div>
                    <div className="form-group">
                      <div className="input-group">
                      <input
                          type="submit"
                          className="form-control"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-6">
                  <div className="box-container">
                    <div className="box">
                      <label>Gross-Income:</label>
                      <input type="text" className="principal transparent-text-box" readOnly value={payslipInfo.gross}/>
                    </div>
                    <div className="box">
                      <label>Income Tax:</label>
                      <input type="text" className="principal transparent-text-box" readOnly value={payslipInfo.tax}/>
                    </div>
                    <div className="box">
                      <label>Net Income:</label>
                      <input type="text" className="interest transparent-text-box" readOnly value={payslipInfo.net}/>
                    </div>
                    <div className="box">
                      <label>Super Amount</label>
                      <input type="text" className="term  transparent-text-box" readOnly value={payslipInfo.super}/>
                    </div>
                  </div>
                </div>
                <div className="col-sm-6 card">
                  <h2 className="panel-heading">Other Info</h2>
                  <ul>
                    <li>
                      <span className="col-sm-6">Full Name:</span><span className="pull-right"><input type="text" className="principal transparent-text-box" readOnly value={payslipInfo.fullName}/></span>
                    </li>
                    <li>
                      <span className="col-sm-6">Peroid:</span><span className="pull-right"><input type="text" className="interest transparent-text-box" readOnly value={payslipInfo.period}/></span>
                    </li>
                  </ul>
                </div>
              </div>
            </form>
          </div>
        </div>
      </section>
    );
  }
}

export default HomeComponent;
